# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-10 14:56:16
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-10 21:48:56
origin_data_path = './origin_data/'
new_data_path = './new_data/'

import numpy as np
import pdb



'''
生成新的bookmarks.dat文件
生成新旧bookmark对应字典文件old_new_bookmarks.dat文件
生成bookmark对应字典old_new_bookmarks
'''

origin_file = open(origin_data_path+'bookmarks.dat','r')#旧bookmark_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'bookmarks.dat','w')#新bookmark_id文件
new_file.write(first_line)#写入表头

old_new_file = open(new_data_path+'old_new_bookmarks.dat','w')#新旧bookmark_id对应文件
old_new_file.write('old_id'+'\t'+'new_id'+'\r\n')

old_new_bookmarks = {}
bookmark_id = 0
for line in origin_file:
	arr = line.strip().split('\t')
	if int(arr[0]) not in old_new_bookmarks:
		bookmark_id += 1
		old_new_bookmarks.setdefault(int(arr[0]),int(bookmark_id))
		old_new_file.write(str(int(arr[0]))+'\t'+str(bookmark_id)+'\r\n')	
	new_file.write(str(bookmark_id)+'\t'+str(arr[1])+'\t'+str(arr[2])+'\t'+str(arr[3])+'\t'+str(arr[4])+'\t'+str(arr[5])+'\r\n')
	
origin_file.close()
new_file.close()
old_new_file.close()



'''
生成新的tags.dat文件
生成新旧tag对应字典文件old_new_tags.dat文件
生成tag对应字典old_new_tags
'''
origin_file = open(origin_data_path+'tags.dat','r')#旧tag_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'tags.dat','w')#新tag_id文件
new_file.write(first_line)#写入表头

old_new_file = open(new_data_path+'old_new_tags.dat','w')#新旧tag_id对应文件
old_new_file.write('old_id'+'\t'+'new_id'+'\r\n')

old_new_tags = {}
tag_id = 0
for line in origin_file:
	arr = line.strip().split('\t')
	
	if int(arr[0]) not in old_new_tags:
		tag_id += 1
		old_new_tags.setdefault(int(arr[0]),int(tag_id))
		old_new_file.write(str(int(arr[0]))+'\t'+str(tag_id)+'\r\n')
	new_file.write(str(tag_id)+'\t'+str(arr[1])+'\r\n')
	
origin_file.close()
new_file.close()
old_new_file.close()


'''
生成新的bookmark_tags.dat文件
'''
origin_file = open(origin_data_path+'bookmark_tags.dat','r')#旧tag_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'bookmark_tags.dat','w')#新tag_id文件
new_file.write(first_line)#写入表头
for line in origin_file:
	arr = line.strip().split('\t')
	new_file.write(str(old_new_bookmarks[int(arr[0])])+'\t'+str(old_new_tags[int(arr[1])])+'\t'+str(arr[2])+'\r\n')
origin_file.close()
new_file.close()



'''
生成新的user_taggedbookmarks-timestamps.dat文件
'''
origin_file = open(origin_data_path+'user_taggedbookmarks-timestamps.dat','r')#旧tag_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'user_taggedbookmarks-timestamps.dat','w')#新tag_id文件
new_file.write(first_line)#写入表头

old_new_file = open(new_data_path+'old_new_users.dat','w')#新旧tag_id对应文件
old_new_file.write('old_id'+'\t'+'new_id'+'\r\n')

old_new_users = {}
user_id = 0

for line in origin_file:
	arr = line.strip().split('\t')
	if int(arr[0]) not in old_new_users:
		user_id += 1
		old_new_users.setdefault(int(arr[0]),int(user_id))
		old_new_file.write(str(int(arr[0]))+'\t'+str(user_id)+'\r\n')
	new_file.write(str(user_id)+'\t'+str(old_new_bookmarks[int(arr[1])])+'\t'+str(old_new_tags[int(arr[2])])+'\t'+str(arr[3])+'\r\n')
origin_file.close()
new_file.close()
old_new_file.close()

'''
生成新的user_taggedbookmarks.dat文件
'''
origin_file = open(origin_data_path+'user_taggedbookmarks.dat','r')#旧tag_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'user_taggedbookmarks.dat','w')#新tag_id文件
new_file.write(first_line)#写入表头


for line in origin_file:
	arr = line.strip().split('\t')
	new_file.write(str(old_new_users[int(arr[0])])+'\t'+str(old_new_bookmarks[int(arr[1])])+'\t'+str(old_new_tags[int(arr[2])])
					+'\t'+str(arr[3])+'\t'+str(arr[4])+'\t'+str(arr[5])+'\t'+str(arr[6])+'\t'+str(arr[7])+'\t'+str(arr[8])+'\r\n')
origin_file.close()
new_file.close()

'''
生成新的user_contacts-timestamps.dat文件
'''
origin_file = open(origin_data_path+'user_contacts-timestamps.dat','r')#旧tag_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'user_contacts-timestamps.dat','w')#新tag_id文件
new_file.write(first_line)#写入表头

for line in origin_file:
	arr = line.strip().split('\t')
	new_file.write(str(old_new_users[int(arr[0])])+'\t'+str(old_new_users[int(arr[1])])+'\t'+str(arr[2])+'\r\n')

origin_file.close()
new_file.close()

'''
生成新的user_contacts.dat文件
'''
origin_file = open(origin_data_path+'user_contacts.dat','r')#旧tag_id文件
first_line = origin_file.readline()#读取表头

new_file = open(new_data_path+'user_contacts.dat','w')#新tag_id文件
new_file.write(first_line)#写入表头

for line in origin_file:
	arr = line.strip().split('\t')
	new_file.write(str(old_new_users[int(arr[0])])+'\t'+str(old_new_users[int(arr[1])])+'\t'+str(arr[2])
					+'\t'+str(arr[3])+'\t'+str(arr[4])+'\t'+str(arr[5])+'\t'+str(arr[6])+'\t'+str(arr[7])+'\r\n')
origin_file.close()
new_file.close()
