# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-16 15:08:38
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-16 15:39:41
"""
对用户和书签的流行度进行从大到小排序
输入文件是user_taggedbookmarks-timestamps_filter.dat
生成用户排名文件user_rank.dat 
书签排名文件bookmark_rank.dat
"""
import numpy as np
import pdb
from operator import itemgetter
data_path = './new_data/user_taggedbookmarks-timestamps_filter.dat'
fin = open(data_path,'r')
fin.readline()
user_rank = {}
bookmark_rank = {}
for line in fin:
	arr = line.strip().split('\t')
	if int(arr[0]) not in user_rank:
		user_rank.setdefault(int(arr[0]),0)
	if int(arr[1]) not in bookmark_rank:
		bookmark_rank.setdefault(int(arr[1]),0)
	user_rank[int(arr[0])] += 1
	bookmark_rank[int(arr[1])] += 1


user_rank_file =  './new_data/user_rank.dat'
fout = open(user_rank_file,'w')
fout.write('userID'+'\n')
for key,value in sorted(user_rank.iteritems(),key = itemgetter(1),reverse=True)[:]:#从大到小排完序
	fout.write(str(key)+'\n')
fout.close()

bookmark_rank_file = './new_data/bookmark_rank.dat'
fout = open(bookmark_rank_file,'w')
fout.write('bookmarkID'+'\n')
for key,value in sorted(bookmark_rank.iteritems(),key = itemgetter(1),reverse=True)[:]:#从大到小排完序
	fout.write(str(key)+'\n')
fout.close()