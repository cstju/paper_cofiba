# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-09 23:01:26
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-16 12:10:39
import sys
from sets import Set
import random

root_path = '/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/paper_COFIBA/'
relationFileName = root_path+'1_data_preprocess/Delicious_dataset/new_data/'
user_tag_bookmark_timestamp_file = relationFileName+'user_taggedbookmarks-timestamps_filter.dat'#


user_arm_tag = []

#remove duplicate events
fin = open(user_tag_bookmark_timestamp_file, 'r')
fin.readline()
#last = {}
for line in fin:	
	arr = line.strip().split('\t')
	t = {}
	t['uid'] = int(arr[0])
	t['aid'] = int(arr[1])	
	t['tstamp'] = int(arr[3])
	#if not t == last:
	#	last = t
	#	user_arm_tag.append(t)
	user_arm_tag.append(t)
print 'event number: '+str(len(user_arm_tag))
fin.close()
#get all arms
arm_pool = Set([])
for t in user_arm_tag:
	arm_pool.add(t['aid'])


#generate random arm_pool and write to file
fout = open(relationFileName+'processed_events.dat','w')
fout.write('userid	timestamp	arm_pool')
for t in user_arm_tag:
	random_pool = [t['aid']]+random.sample(arm_pool, 24)
	fout.write(str(t['uid'])+'\t'+str(t['tstamp'])+'\t'+str(random_pool)+'\n')
fout.close()

