# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-17 10:35:43
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-17 21:22:07
import sys
from sets import Set
import numpy as np
import pdb
import random
def constructAdjMatrix(inputfile):#构建用户-用户之间相似度矩阵
	fin = open(inputfile,'r')
	arr0 = fin.readline().split(' ')
	#print arr0
	n = int(arr0[0])
	#print n
	#pdb.set_trace()
	G = np.identity(n)#矩阵G的行列数都是用户总数
	user_i_id = 0
	for line in fin :
		
		sSim = 1.0
		arr = line.strip().split(' ')
		#arr.append(str(user_i_id+1))
		#print type(arr[0])
		if arr[0] != '':
			for user_j_id in arr:
				user_j_id = int(user_j_id) - 1
				G[user_i_id][user_j_id] = 1.0
				#print 'aaa'
				sSim += 1.0
		G[user_i_id] /= sSim
		user_i_id += 1
	return G











	n = len(self.users)	
	G = np.identity(shape = (n, n))#矩阵G的行列数都是用户总数
	for ui in self.users:
		sSim = 0
		for uj in self.users:
			sim = np.dot(ui.theta, uj.theta)#计算用户i和用户j之间的相似度
			if ui.id == uj.id:
				sim *= 1.0
			G[ui.id][uj.id] = sim
			sSim += sim
			
		G[ui.id] /= sSim#进行归一化处理，目标用户与其他用户的相似度和为1
		'''
		for i in range(n):
			print '%.3f' % G[ui.id][i],
		print ''
		'''
	#G = 1.0/n*np.ones(shape = (n, n))
	#G = np.identity(n)
	return G

	# top m users
def constructSparseMatrix(self, m):#得到与目标用户相似最高的前m个相似用户，其他用户与目标用户的相似度为0
	n = len(self.users)	
	G = np.zeros(shape = (n, n))
	for ui in self.users:
		sSim = 0
		for uj in self.users:
			sim = np.dot(ui.theta, uj.theta)
 			if ui.id == uj.id:
 				sim *= 1.0
			G[ui.id][uj.id] = sim
			sSim += sim		
		G[ui.id] /= sSim
	for ui in self.users:
		similarity = sorted(G[ui.id], reverse=True)#从大到小排序
		threshold = similarity[m]				
		
		# trim the graph
		for i in range(n):
			if G[ui.id][i] <= threshold:
				G[ui.id][i] = 0;
		G[ui.id] /= sum(G[ui.id])#进行归一化处理，目标用户与其他用户的相似度和为1
		'''
		for i in range(n):
			print '%.3f' % G[ui.id][i],
		print ''
		'''
	return G
	# create user connectivity graph
def initializeW(self, sparseLevel, epsilon):	
	n = len(self.users)	
	if sparseLevel >=n or sparseLevel<=0:#如果sparseLevel的值异常
 		W = self.constructAdjMatrix()#权重W的值为用户之间相似度
 	else:#如果sparseLevel的值不异常
 		W = self.constructSparseMatrix(sparseLevel)#权重
 	#print 'W.T', W.T
	return W.T
def initializeW0(self,W):#W0是在W基础上加上噪声并且进行了归一化处理
	W0 = W.copy()
	#print 'WWWWWWWWWW0', W0
	for i in range(W.shape[0]):
		for j in range(W.shape[1]):
			W0[i][j] = W[i][j] + self.matrixNoise()#
			if W0[i][j] < 0:
				W0[i][j] = 0
		W0[i] /= sum(W0[i]) 
	#W0 = np.random.random((W.shape[0], W.shape[1]))  #test random ini
	#print 'W0.T', W0.T

	return W0.T
"""
程序里用到的谱聚类方法Graph，参考博客：http://blog.csdn.net/v_july_v/article/details/40738211
谱聚类算法过程:
(1)根据数据构造一个Graph，Graph的每一个节点对应一个数据点，
   将各个点连接起来（随后将那些已经被连接起来但并不怎么相似的点，通过cut/RatioCut/NCut 的方式剪开),
   并且边的权重用于表示数据之间的相似度。
   把这个Graph用邻接矩阵的形式表示出来,记为W。
(2)把W的每一列元素加起来得到N个数，把它们放在对角线上（其他地方都是零），
   组成一个NxN的对角矩阵,记为度矩阵D。并把W-D的结果记为拉普拉斯矩阵L=W-D。
(3)求出L的前k个特征值（前k个指按照特征值的大小从小到大排序得到)λik,以及对应的特征向量Vik。
(4)把这k个特征（列）向量Vik排列在一起组成一个Nxk的矩阵，将其中每一行看作k维空间中的一个向量，
   并使用 K-means 算法进行聚类。聚类的结果中每一行所属的类别就是原来 Graph 中的节点亦即最初的个数据点分别所属的类别。
   或许你已经看出来，谱聚类的基本思想便是利用样本数据之间的相似矩阵（拉普拉斯矩阵）
进行特征分解（ 通过Laplacian Eigenmap 的降维方式降维），然后将得到的特征向量进行 K-means聚类。
   此外，谱聚类和传统的聚类方法（例如 K-means）相比，谱聚类只需要数据之间的相似度矩阵就可以了，
而不必像K-means那样要求数据必须是 N 维欧氏空间中的向量。
"""
def initializeGW(self,G, Gepsilon):
	"""
	>>> G = np.arange(5) * np.arange(5)[:, np.newaxis]
	>>> G
	array([[ 0,  0,  0,  0,  0],
	       [ 0,  1,  2,  3,  4],
	       [ 0,  2,  4,  6,  8],
	       [ 0,  3,  6,  9, 12],
	       [ 0,  4,  8, 12, 16]])
	>>> csgraph.laplacian(G, normed=False)
	array([[  0,   0,   0,   0,   0],
	       [  0,   9,  -2,  -3,  -4],
	       [  0,  -2,  16,  -6,  -8],
	       [  0,  -3,  -6,  21, -12],
	       [  0,  -4,  -8, -12,  24]])
	>>> G = np.array([[1,0,1,1,0],[0,1,0,1,1],[1,0,1,1,0],[1,1,1,1,0],[0,1,0,0,1]])
	>>> print G
	[[1 0 1 1 0]
	 [0 1 0 1 1]
	 [1 0 1 1 0]
	 [1 1 1 1 0]
	 [0 1 0 0 1]]
	>>> print csgraph.laplacian(G, normed=False)
	[[ 2  0 -1 -1  0]
	 [ 0  2  0 -1 -1]
	 [-1  0  2 -1  0]
	 [-1 -1 -1  3  0]
	 [ 0 -1  0  0  1]]
	"""
	n = len(self.users)	
	L = csgraph.laplacian(G, normed = False)#计算G矩阵的拉普拉斯矩阵
	I = np.identity(n = G.shape[0]) 
	#Gepsilon是一个系数，在论文中为1
	GW = I + Gepsilon*L  # W is a double stochastic matrix
	#print 'GW', GW
	return GW.T

def getW(self):
	return self.W
def getW0(self):
	return self.W0
def getGW(self):
	return self.GW
def getGW0(self):
	return self.GW0
if __name__ == '__main__':
	root_path = '/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/paper_COFIBA/'
	relationfile = root_path+'2_extract_feature/Delicious/user_cluster/user_relation_adjacency_list.dat'

	G = constructAdjMatrix(relationfile)
	print G[1]
	print sum(G[1])
	print G[1][1]
	print type(G)
	print type(G[1])
	print type(G[1][1])
	print G[2]
	print sum(G[2])
	print G[2][2]
	print type(G)
	print type(G[2])
	print type(G[2][2])




