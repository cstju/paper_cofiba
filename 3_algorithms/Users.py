# -*- coding: utf-8 -*-
import numpy as np 
from util_functions import featureUniform, gaussianFeature, fileOverWriteWarning
import json
from random import choice, randint
import pdb
"""
本程序文件下的函数主要是对用户方面的数据进行读写操作
"""
class User():
	def __init__(self, uid, theta = None, CoTheta = None):
		self.id = uid
		self.theta = theta
		self.CoTheta = CoTheta
		self.uncertainty = 0.0
	def updateUncertainty(uncertainty):
		self.uncertainty = uncertainty


class UserManager():
	def __init__(self, dimension, userNum,  UserGroups, thetaFunc, argv = None):
		self.dimension = dimension
		self.thetaFunc = thetaFunc#这里的thetaFunc是一个函数，初始输入什么函数就用什么函数去计算
		self.userNum = userNum
		self.UserGroups = UserGroups
		self.argv = argv
		self.signature = "A-"+"+PA"+"+TF-"+self.thetaFunc.__name__

	def saveUsers(self, users, filename, force = False):
		fileOverWriteWarning(filename, force)
		with open(filename, 'w') as f:
			for i in range(len(users)):
				#print users[i].theta
				f.write(json.dumps((users[i].id, users[i].theta.tolist())) + '\n')
				
	def loadUsers(self, filename):#从指定文件中输入用户特征
		users = []
		with open(filename, 'r') as f:
			for line in f:
				uid, theta = json.loads(line)
				users.append(User(uid, np.array(theta)))
		return users#返回的users中的每一行是类User的一个对象

	def generateMasks(self):
		mask = {}#用户聚类集字典，键值对个数等于用户聚类集个数，每一个键值对中，值是一个维度为dimension的列向量
		for i in range(self.UserGroups):
			mask[i] = np.random.randint(2, size = self.dimension)#生成维度为dimension的随机0，1列向量
		return mask


	def simulateThetafromUsers(self):#此函数是先采用高斯分布，并做归一化处理随机生成用户特征偏置
		#usersids = {}
		users = []
		#mask = self.generateMasks()

		for i in range(self.userNum):
			thetaVector = self.thetaFunc(self.dimension, argv = self.argv)
			l2_norm = np.linalg.norm(thetaVector, ord =2)#将向量中元素的元素取平方然后求和，最后开平方
			users.append(User(i, thetaVector/l2_norm))
			#print users 如果输出users的话,输出的为地址
			#pdb.set_trace()
			#print "aaaaa"

		return users#返回的users中的每一行是类User的一个对象，这么做应该是为了省内存
	"""
	def simulateThetafromUsers(self):#此函数是先采用高斯分布，并做归一化处理随机生成用户特征偏置
		usersids = {}
		users = []
		mask = self.generateMasks()

		for i in range(self.UserGroups):
			usersids[i] = range(self.userNum*i/self.UserGroups, (self.userNum*(i+1))/self.UserGroups)

			for key in usersids[i]:
				thetaVector = np.multiply(self.thetaFunc(self.dimension, argv = self.argv), mask[i])
				l2_norm = np.linalg.norm(thetaVector, ord =2)#将向量中元素的元素取平方然后求和，最后开平方
				users.append(User(key, thetaVector/l2_norm))
				#print users 如果输出users的话,输出的为地址
				#pdb.set_trace()
				#print "aaaaa"

		return users#返回的users中的每一行是类User的一个对象，这么做应该是为了省内存
	"""