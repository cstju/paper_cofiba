# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-13 22:36:03
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-17 21:07:02

import os

root_path = '/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/paper_COFIBA/'

user_friends_file = root_path+'2_extract_feature/Delicious/user_cluster/user_relation_adjacency_list.dat'

process_event_file = root_path+'1_data_preprocess/Delicious_dataset/new_data/processed_events.dat'

result_folder = "./SimulationResults/"
Delicious_result = result_folder+"Delicious_results"
Lastfm_result = result_folder+"Last.fm_results"
Movielens_result = result_folder+"Movielens_results"

intermediate_result_folder = "./Simulation_intermediate_result/"
Delicious_intermediate_result = intermediate_result_folder+"Delicious_results"
Lastfm_intermediate_result = intermediate_result_folder+"Last.fm_results"
Movielens_intermediate_result = intermediate_result_folder+"Movielens_results"

Delicious_feature_folder = root_path+"2_extract_feature/Delicious/feature_result/"
Delicious_feature = Delicious_feature_folder+"feature_vectors.dat"#这个文件要用scipy.io.mmread()打开
Delicious_itemid = Delicious_feature_folder+"arm_idx.dat"




