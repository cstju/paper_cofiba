# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-09 21:27:48
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-14 22:57:20

import numpy as np
from scipy.linalg import sqrtm
import math
import time
import datetime

from util_functions import vectorize, matrixize
from CoLin import CoLinAlgorithm, CoLin_SelectUserAlgorithm
"""
本程序是参照论文 A Gang of Bandits 进行的编写
程序中的公式可以跟论文中进行一一对应（但是先后有点顺序不同，程序应该是对的）
在这个程序中，并没有用到用户特征向量，
这里的用户特征向量self.theta实际上是由两个矩阵self.AInv和self.b相乘得到的
"""



class GOBLinSharedStruct:
	def __init__(self, featureDimension, lambda_, userNum, W, RankoneInverse):
		self.W = W#这里的W矩阵已经经过laplace变换了
		self.userNum = userNum
		self.A = lambda_*np.identity(n = featureDimension*userNum)#lambda_在论文中的值为1
		self.b = np.zeros(featureDimension*userNum)
		self.AInv = np.linalg.inv(self.A)
		"""
		>>> np.kron([5,6,7], [1,10,100])
		array([  5,  50, 500,   6,  60, 600,   7,  70, 700])
		"""
		"""
		>>> np.kron(np.eye(2), np.ones((2,2)))
		array([[ 1.,  1.,  0.,  0.],
			   [ 1.,  1.,  0.,  0.],
			   [ 0.,  0.,  1.,  1.],
			   [ 0.,  0.,  1.,  1.]])
		"""

		self.theta = np.dot(self.AInv , self.b)
		self.STBigWInv = sqrtm(np.linalg.inv(np.kron(W, np.identity(n=featureDimension))) )
		self.STBigW = sqrtm(np.kron(W, np.identity(n=featureDimension)))
		self.RankoneInverse = RankoneInverse
	def updateParameters(self, articlePicked, click, userID):
		featureVectorM = np.zeros(shape =(len(articlePicked.featureVector), self.userNum))
		featureVectorM.T[userID] = articlePicked.featureVector
		featureVectorV = vectorize(featureVectorM)#将行数为d列数为n的特征矩阵转化为一个向量

		CoFeaV = np.dot(self.STBigWInv, featureVectorV)
		self.A = self.A + np.outer(CoFeaV, CoFeaV)
		self.b = self.b + click * CoFeaV

		if self.RankoneInverse:
			temp = np.dot(self.AInv, CoFeaV)
			self.AInv = self.AInv - (np.outer(temp,temp))/(1.0+np.dot(np.transpose(CoFeaV),temp))
		else:
			self.AInv =  np.linalg.inv(self.A)
	
		self.theta = np.dot(self.AInv, self.b)
	def getProb(self,alpha , article, userID):
		
		featureVectorM = np.zeros(shape =(len(article.featureVector), self.userNum))
		featureVectorM.T[userID] = article.featureVector
		featureVectorV = vectorize(featureVectorM)

		CoFeaV = np.dot(self.STBigWInv, featureVectorV)
		
		mean = np.dot(np.transpose(self.theta), CoFeaV)		
		a = np.dot(CoFeaV, self.AInv)
		var = np.sqrt( np.dot( a , CoFeaV))
		
		pta = mean + alpha * var
		
		return pta

class GOBLinAlgorithm:
	def __init__(self, dimension, alpha, lambda_, n, W, RankoneInverse = False):
		"""
		GOBLin算法写的比较简单的原因是它是继承了CoLin算法
		有一些函数如参数更新等函数直接可以使用CoLin算法
		中的函数
		"""
		self.USERS = GOBLinSharedStruct(dimension, lambda_, n, W, RankoneInverse)

		self.dimension = dimension
		self.alpha = alpha
		self.W = W

		self.CanEstimateCoUserPreference = True 
		self.CanEstimateUserPreference = False
		self.CanEstimateW = False

	def decide(self, pool_articles, userID):
		maxPTA = float('-inf')
		articlePicked = None

		for x in pool_articles:
			x_pta = self.USERS.getProb(self.alpha, x, userID)
			# pick article with highest Prob
			if maxPTA < x_pta:
				articlePicked = x
				maxPTA = x_pta

		return articlePicked
	def updateParameters(self, articlePicked, click, userID):
		self.USERS.updateParameters(articlePicked, click, userID)
		
	def getTheta(self, userID):
		return self.USERS.UserTheta.T[userID]

	def getCoTheta(self, userID):
		thetaMatrix =  matrixize(self.USERS.theta, self.dimension) 
		return thetaMatrix.T[userID]

	def getA(self):
		return self.USERS.A


class GOBLin_SelectUserAlgorithm:
	def __init__(self, dimension, alpha, lambda_, n, W, RankoneInverse = False):
		self.USERS = GOBLinSharedStruct(dimension, lambda_, n, W, RankoneInverse)
		self.dimension = dimension
		self.alpha = alpha
		self.W = W

	def decide(self, pool_articles, AllUsers):
		maxPTA = float('-inf')
		articlePicked = None
		userPicked = None

		for x in pool_articles:
			for user in AllUsers:
				x_pta = self.USERS.getProb(self.alpha, x, user.id)
				# pick article with highest Prob
				if maxPTA < x_pta:
					articlePicked = x
					userPicked = user
					maxPTA = x_pta

		return userPicked,articlePicked

	def updateParameters(self, articlePicked, click, userID):
		self.USERS.updateParameters(articlePicked, click, userID)


	def getTheta(self, userID):
		return self.USERS.UserTheta.T[userID]

	def getCoTheta(self, userID):
		thetaMatrix =  matrixize(self.USERS.theta, self.dimension) 
		return thetaMatrix.T[userID]

	def getA(self):
		return self.USERS.A

