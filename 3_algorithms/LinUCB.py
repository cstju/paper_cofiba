# -*- coding: utf-8 -*-
import numpy as np
from scipy.linalg import sqrtm
import math
from util_functions import vectorize, matrixize
import pdb

class LinUCBUserStruct:
	"""
	这个算法思想公式来自于论文 A Contextual-Bandit Approach to Personalized News Article Recommendation
	中基本的LinUCB算法，程序中的公式与论文中的公式完全符合
	"""
	def __init__(self, featureDimension, userID, lambda_, RankoneInverse):
		self.userID = userID
		self.A = lambda_*np.identity(n = featureDimension)
		self.b = np.zeros(featureDimension)
		self.AInv = np.linalg.inv(self.A)#对矩阵A取逆
		self.UserTheta = np.zeros(featureDimension)#单一用户的偏置向量，维度为特征个数
		self.RankoneInverse = RankoneInverse


	def updateParameters(self, featureVector, click):
		#关键在于click这个向量参数是怎么设定的
		"""
		outer()函数是给两个向量，每个元素都相互相乘得到矩阵
		Given two vectors, a = [a0, a1, ..., aM] and b = [b0, b1, ..., bN], the outer product is:
		[[a0*b0  a0*b1 ... a0*bN ]
		 [a1*b0    .
		 [ ...          .
		 [aM*b0            aM*bN ]]
		"""
		self.A += np.outer(featureVector, featureVector)
		self.b += featureVector*click
		if self.RankoneInverse:
			temp = np.dot(self.AInv, featureVector)
			self.AInv = self.AInv - (np.outer(temp,temp))/(1.0+np.dot(np.transpose(featureVector),temp))
		else:
			self.AInv = np.linalg.inv(self.A)
		self.UserTheta = np.dot(self.AInv, self.b)
		
	def getTheta(self):
		return self.UserTheta
	
	def getA(self):
		return self.A

	def getProb(self, alpha, users, featureVector):
		mean = np.dot(self.getTheta(), featureVector)
		var = np.sqrt(np.dot(np.dot(featureVector, np.linalg.inv(self.getA())), featureVector))
		pta = mean + alpha * var
		return pta


class Hybrid_LinUCB_singleUserStruct(LinUCBUserStruct):#针对单一用户的Hybrid_LinUCB算法,
#是对LinUCB算法继承
	def __init__(self, userFeature, lambda_, userID, RankoneInverse=False):
		LinUCBUserStruct.__init__(self, len(userFeature), userID,  lambda_, RankoneInverse)
		self.d = len(userFeature)
		
		self.B = np.zeros([self.d, self.d**2])
		self.userFeature = userFeature
	def updateParameters(self, featureVector, click):
		article_FeatureVector = featureVector

		#additionalFeatureVector的长度为用户特征个数的平方
		z = vectorize(np.outer(self.userFeature, article_FeatureVector))
		LinUCBUserStruct.updateParameters(self, article_FeatureVector, click)#这里更新参数调用的是类LinUCBUserStruct里的更新参数
		self.B +=np.outer(article_FeatureVector, z)
	def updateTheta(self, beta):
		self.UserTheta = np.dot(self.AInv, (self.b- np.dot(self.B, beta)))

"""
Hybrid_LinUCBAlgorithms算法用到了用户特征向量
但是实际上此用户特征向量是由W矩阵通过svd得到的
"""

class Hybrid_LinUCBUserStruct:
	def __init__(self, featureDimension, lambda_, userFeatureList, RankoneInverse=False):
		self.k = featureDimension**2
		self.A_z = lambda_*np.identity(n = self.k)
		self.b_z = np.zeros(self.k)
		self.A_zInv = np.linalg.inv(self.A_z)
		self.beta = np.dot(self.A_zInv, self.b_z)
		self.users = []

		for i in range(len(userFeatureList)):
			#这里没有重复定义参数，而是使用的Hybrid_LinUCB_singleUserStruct中已经定义的参数
			#因为有些参数是每一个用户所特有的，所以声明的时候就调用前面的类Hybrid_LinUCB_singleUserStructß
			self.users.append(Hybrid_LinUCB_singleUserStruct(userFeatureList[i], lambda_ , i, RankoneInverse))

	def updateParameters(self, featureVector, click, userID):
		#参数click是一个单一值，不是矩阵也不是向量
		articlePicked_FeatureVector = featureVector
		z = vectorize( np.outer(self.users[userID].userFeature, articlePicked_FeatureVector))
		#np.transpose是对数组进行转置操作
		temp = np.dot(np.transpose(self.users[userID].B), self.users[userID].AInv)

		self.A_z += np.dot(temp, self.users[userID].B)
		self.b_z +=np.dot(temp, self.users[userID].b)

		self.users[userID].updateParameters(articlePicked_FeatureVector, click)#更新用户userID自己特制定的参数

		temp = np.dot(np.transpose(self.users[userID].B), self.users[userID].AInv)

		self.A_z = self.A_z + np.outer(z,z) - np.dot(temp, self.users[userID].B)
		self.b_z =self.b_z+ click*z - np.dot(temp, self.users[userID].b)
		self.A_zInv = np.linalg.inv(self.A_z)

		self.beta =np.dot(self.A_zInv, self.b_z)
		self.users[userID].updateTheta(self.beta)

	def getProb(self, alpha, article_FeatureVector,userID):
		x = article_FeatureVector
		z = vectorize(np.outer(self.users[userID].userFeature, article_FeatureVector))
		temp =np.dot(np.dot(np.dot( self.A_zInv , np.transpose( self.users[userID].B)) , self.users[userID].AInv), x )
		mean = np.dot(self.users[userID].UserTheta,  x)+ np.dot(self.beta, z)
		s_t = np.dot(np.dot(z, self.A_zInv),  z) -2* np.dot(z, temp) + np.dot(np.dot(x, self.users[userID].AInv),  x)
		+ np.dot(np.dot( np.dot(x, self.users[userID].AInv) ,  self.users[userID].B ) ,temp)

		var = np.sqrt(s_t)
		pta = mean + alpha * var
		return pta


class Hybrid_LinUCBAlgorithm(object):
	def __init__(self, dimension, alpha, lambda_, userFeatureList, RankoneInverse = False):
		self.dimension = dimension
		self.alpha = alpha
		self.USER = Hybrid_LinUCBUserStruct(dimension, lambda_, userFeatureList, RankoneInverse)

		
		self.CanEstimateCoUserPreference = True
		self.CanEstimateUserPreference = False
		self.CanEstimateW = False
	def decide(self, pool_articles, userID):
		maxPTA = float('-inf')
		articlePicked = None

		for x in pool_articles:
			x_pta = self.USER.getProb(self.alpha, x.featureVector, userID)
			if maxPTA < x_pta:
				articlePicked = x
				maxPTA = x_pta
		return articlePicked
	def updateParameters(self, featureVector, click, userID):
		self.USER.updateParameters(featureVector, click, userID)
	def getCoTheta(self, userID):
		return self.USER.users[userID].UserTheta




class LinUCBAlgorithm:
	def __init__(self, dimension, alpha, lambda_, n, RankoneInverse = False):  # n is number of users
		self.users = []
		#algorithm have n users, each user has a user structure
		for i in range(n):
			self.users.append(LinUCBUserStruct(dimension, i, lambda_ ,RankoneInverse)) 

		self.dimension = dimension
		self.alpha = alpha

		self.CanEstimateCoUserPreference = True 
		self.CanEstimateUserPreference = False
		self.CanEstimateW = False

	def decide(self, pool_articles, userID):#返回预测概率评分最大的那个文章编号
		maxPTA = float('-inf')
		articlePicked = None

		for x in pool_articles:
			x_pta = self.users[userID].getProb(self.alpha, self.users, x.featureVector)
			# pick article with highest Prob
			if maxPTA < x_pta:
				articlePicked = x
				maxPTA = x_pta

		return articlePicked

	def updateParameters(self, featureVector, click, userID):
		self.users[userID].updateParameters(featureVector, click)
		
	def getCoTheta(self, userID):
		return self.users[userID].UserTheta

class Uniform_LinUCBAlgorithm:#所有的用户使用相同的特征
	def __init__(self, dimension, alpha, lambda_, RankoneInverse = False):
		self.dimension = dimension
		self.alpha = alpha
		self.USER = LinUCBUserStruct(dimension, lambda_, RankoneInverse)

		self.CanEstimateUserPreference = False
		self.CanEstimateCoUserPreference = True 
		self.CanEstimateW = False
	def decide(self, pool_articles, userID):
		maxPTA = float('-inf')
		articlePicked = None

		for x in pool_articles:
			x_pta = self.USER.getProb(self.alpha, self.USER, x.featureVector)
			if maxPTA < x_pta:
				articlePicked = x
				maxPTA = x_pta
		return articlePicked
	def updateParameters(self, featureVector, click, userID):
		self.USER.updateParameters(featureVector, click)
	def getCoTheta(self, userID):
		return self.USER.UserTheta

class LinUCB_SelectUserAlgorithm:
	def __init__(self, dimension, alpha, lambda_, n, RankoneInverse = False):  # n is number of users
		self.users = []
		#algorithm have n users, each user has a user structure
		for i in range(n):
			self.users.append(LinUCBUserStruct(dimension, i, lambda_ , RankoneInverse)) 

		self.dimension = dimension
		self.alpha = alpha

		self.CanEstimateCoUserPreference = True 
		self.CanEstimateUserPreference = False
		self.CanEstimateW = False

	def decide(self, pool_articles, AllUsers):
		maxPTA = float('-inf')
		articlePicked = None
		userPicked = None
		AllUsers = list(np.random.permutation(AllUsers)) 
		for x in pool_articles:
			for user in AllUsers:
				x_pta = self.users[user.id].getProb(self.alpha, self.users, x.featureVector)
				# pick article with highest Prob
				if maxPTA < x_pta:
					articlePicked = x
					userPicked = user
					maxPTA = x_pta

		return userPicked, articlePicked

	def updateParameters(self, featureVector, click, userID):
		self.users[userID].updateParameters(featureVector, click)
		
	def getLearntParameters(self, userID):
		return self.users[userID].UserTheta


