# -*- coding: utf-8 -*-
from collections import Counter
from math import log
import numpy as np 
from random import *
from custom_errors import FileExists 

def gaussianFeature(dimension, argv):
	mean = argv['mean'] if 'mean' in argv else 0#查看字典argv中有无mean的初始值，没有则mean=0
	std = argv['std'] if 'std' in argv else 1#查看字典argv中有无std的初始值，没有则std=1

	mean_vector = np.ones(dimension)*mean
	#np.identity(dimension)建立行列值都为dimension的单位矩阵
	stdev = np.identity(dimension)*std
	vector = np.random.multivariate_normal(np.zeros(dimension), stdev)#建立多元正太分布，维度为dimension

	l2_norm = np.linalg.norm(vector, ord = 2)
	if 'l2_limit' in argv and l2_norm > argv['l2_limit']:#如果初始的l2_limit阶数更高就用初始值
		"This makes it uniform over the circular range"
		vector = (vector / l2_norm)
		vector = vector * (random())
		vector = vector * argv['l2_limit']

	if mean is not 0:
		vector = vector + mean_vector

	vectorNormalized = []
	for i in range(len(vector)):
		vectorNormalized.append(vector[i]/sum(vector))#进行归一化处理
	return vectorNormalized
	#return vector

def featureUniform(dimension, argv = None):
	#random() 是类random中的函数，生成0-1之间的正数
	vector = np.array([random() for _ in range(dimension)])#生成维度是dimension的列向量，值都是0-1之间的随机数

	l2_norm = np.linalg.norm(vector, ord =2)#将向量中元素的元素取平方然后求和，最后开平方
	
	vector = vector/l2_norm#用原向量除以累计平方根，这样确保得到的新向量中的元素平方和为1
	return vector

def getBatchStats(arr):
	return np.concatenate((np.array([arr[0]]), np.diff(arr)))

def checkFileExists(filename):#检查文件是否存在，存在返回1，不存在返回0
	try:
		with open(filename, 'r'):
			return 1
	except IOError:
		return 0 

def fileOverWriteWarning(filename, force):#检查文件是否将会被重复写入
	if checkFileExists(filename):
		if force == True:#当force为True时允许被从新写入并提示
			print "Warning : fileOverWriteWarning %s"%(filename)
		else:#否则认为是一个读写异常，使用raise用来抛出异常，raise后面的程序不再运行
			raise FileExists(filename)


def vectorize(M):#将M矩阵按列转化为一个一维列表
	temp = []
	for i in range(M.shape[0]*M.shape[1]):
		temp.append(M.T.item(i))
	#z最终生成的temp为一个列表
	V = np.asarray(temp)#将列表转化为一维列向量
	return V

def matrixize(V, C_dimension):
	"""
	将V向量转换为W矩阵，C_dimension为行数，有限进行同一列的行填充
	V = [ 3 21  6  4 42  2  5  1  7]
	C_dimension = 3
	W = matrixize(V, C_dimension)
	W = [[  3.   4.   5.]
 		 [ 21.  42.   1.]
		 [  6.   2.   7.]]
	C_dimension = 2
	W = [[  3.   6.  42.   5.]
 		 [ 21.   4.   2.   1.]]
	"""
	temp = np.zeros(shape = (C_dimension, len(V)/C_dimension))
	for i in range(len(V)/C_dimension):
		temp.T[i] = V[i*C_dimension : (i+1)*C_dimension]
	W = temp
	return W

def parseLine_Delicious(line):#对processed_event.dat中的数据进行提取
        userID, tim, pool_articles = line.split("\t")
        userID, tim = int(userID), int(tim)
        pool_articles = np.array(pool_articles.strip('\n').strip('[').strip(']').split(', '))
        return userID, tim, pool_articles

class observation_entry():
	def __init__(self, user, articlePool, OptimalReward, noise):
		self.user = user
		self.articlePool = articlePool
		self.OptimalReward = OptimalReward
		self.noise = noise
