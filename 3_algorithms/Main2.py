# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-16 20:17:03
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-17 21:23:36
import numpy as np
from operator import itemgetter      #for easiness in sorting and finding max and stuff
from matplotlib.pylab import *
import matplotlib
matplotlib.use('Agg')
from random import sample, choice
from scipy.sparse import csgraph 
import os
# local address to save simulated users, simulated articles, and results
from conf import *
from util_functions import *
from Articles import *
from Users import *
from CLUB import *
from LinUCB import *
from CoLin import *
from GOBLin import *
from COFIBA import *
from W_Alg import *
from eGreedyUCB1 import *
from scipy.linalg import sqrtm
import scipy.io
import math
import argparse
import pdb

from sklearn.decomposition import TruncatedSVD


class simulateOnlineData():
	def __init__(self, dimension, iterations, articles, users, 
					batchSize = 1000,
					noise = lambda : 0,
					matrixNoise = lambda : 0,
					type_ = 'UniformTheta', 
					signature = '', 
					poolArticleSize = 10, 
					noiseLevel = 0, 
					matrixNoiseLevel =0,
					epsilon = 1, 
					Gepsilon = 1, 
					sparseLevel=0):

		self.simulation_signature = signature
		self.type = type_

		self.dimension = dimension
		self.iterations = iterations

		self.noise = noise#因为这里的self.noise是一个函数，所以每次会生成一个噪声随机数，而不是固定值用于在单值中引入噪声
		self.matrixNoise = matrixNoise#因为这里的self.matrixNoise是一个函数，所以每次会生成一个噪声随机数，而不是固定值用于在矩阵中引入噪声

		self.articles = articles
		self.users = users

		self.poolArticleSize = poolArticleSize
		self.batchSize = batchSize

		self.noiseLevel = noiseLevel#单一值所加的噪声因素的标准差值为noiseLevel
		self.matrixNoiseLevel = matrixNoiseLevel#矩阵所加的噪声因素的标准差值为matrixNoiseLevel
		
		self.W = self.initializeW(sparseLevel,epsilon)#在创建的时候就已经计算用户之间的相似度矩阵
		W = self.W.copy()
		self.W0 = self.initializeW0(W)
		W0 = self.W0.copy()

		self.GW = self.initializeGW(W,Gepsilon)
		self.GW0 = self.initializeGW(W0,Gepsilon)
		

		self.sparseLevel = sparseLevel#sparseLevel为选取与目标用户最相似的前sparseLevel个用户，sparseLevel值应该大于0小于元素总数
	"""
	################################################################
	##########采用用户之间相互关注文件构建用户-用户相似度矩阵#############
	################################################################
	def constructAdjMatrix():#构建用户-用户之间相似度矩阵
		fin = open(inputfile,'r')
		arr0 = fin.readline().split(' ')
		n = int(arr0[0])
		G = np.identity(n)#矩阵G的行列数都是用户总数
		user_i_id = 0
		for line in fin :
			sSim = 1.0
			arr = line.strip().split(' ')
			if arr[0] != '':
				for user_j_id in arr:
					user_j_id = int(user_j_id) - 1
					G[user_i_id][user_j_id] = 1.0
					sSim += 1.0
			G[user_i_id] /= sSim
			user_i_id += 1
		return G
	"""
	def constructAdjMatrix(self):#构建用户-用户之间相似度矩阵
		n = len(self.users)	

		G = np.zeros(shape = (n, n))#矩阵G的行列数都是用户总数
		for ui in self.users:
			sSim = 0
			for uj in self.users:
				sim = np.dot(ui.theta, uj.theta)#计算用户i和用户j之间的相似度
 				if ui.id == uj.id:
 					sim *= 1.0
				G[ui.id][uj.id] = sim
				sSim += sim
				
			G[ui.id] /= sSim#进行归一化处理，目标用户与其他用户的相似度和为1
			'''
			for i in range(n):
				print '%.3f' % G[ui.id][i],
			print ''
			'''
		#G = 1.0/n*np.ones(shape = (n, n))
		#G = np.identity(n)
		return G
    
    # top m users
	def constructSparseMatrix(self, m):#得到与目标用户相似最高的前m个相似用户，其他用户与目标用户的相似度为0
		n = len(self.users)	

		G = np.zeros(shape = (n, n))
		for ui in self.users:
			sSim = 0
			for uj in self.users:
				sim = np.dot(ui.theta, uj.theta)
 				if ui.id == uj.id:
 					sim *= 1.0
				G[ui.id][uj.id] = sim
				sSim += sim		
			G[ui.id] /= sSim
		for ui in self.users:
			similarity = sorted(G[ui.id], reverse=True)#从大到小排序
			threshold = similarity[m]				
			
			# trim the graph
			for i in range(n):
				if G[ui.id][i] <= threshold:
					G[ui.id][i] = 0;
			G[ui.id] /= sum(G[ui.id])#进行归一化处理，目标用户与其他用户的相似度和为1

			'''
			for i in range(n):
				print '%.3f' % G[ui.id][i],
			print ''
			'''
		return G

		

	# create user connectivity graph
	def initializeW(self, sparseLevel, epsilon):	
		n = len(self.users)	
		if sparseLevel >=n or sparseLevel<=0:#如果sparseLevel的值异常
 			W = self.constructAdjMatrix()#权重W的值为用户之间相似度
 		else:#如果sparseLevel的值不异常
 			W = self.constructSparseMatrix(sparseLevel)#权重
 		#print 'W.T', W.T
		return W.T
	def initializeW0(self,W):#W0是在W基础上加上噪声并且进行了归一化处理
		W0 = W.copy()
		#print 'WWWWWWWWWW0', W0
		for i in range(W.shape[0]):
			for j in range(W.shape[1]):
				W0[i][j] = W[i][j] + self.matrixNoise()#
				if W0[i][j] < 0:
					W0[i][j] = 0
			W0[i] /= sum(W0[i]) 
		#W0 = np.random.random((W.shape[0], W.shape[1]))  #test random ini
		#print 'W0.T', W0.T

		return W0.T
	"""
	程序里用到的谱聚类方法Graph，参考博客：http://blog.csdn.net/v_july_v/article/details/40738211
	谱聚类算法过程:
	(1)根据数据构造一个Graph，Graph的每一个节点对应一个数据点，
	   将各个点连接起来（随后将那些已经被连接起来但并不怎么相似的点，通过cut/RatioCut/NCut 的方式剪开),
	   并且边的权重用于表示数据之间的相似度。
	   把这个Graph用邻接矩阵的形式表示出来,记为W。
	(2)把W的每一列元素加起来得到N个数，把它们放在对角线上（其他地方都是零），
	   组成一个NxN的对角矩阵,记为度矩阵D。并把W-D的结果记为拉普拉斯矩阵L=W-D。
	(3)求出L的前k个特征值（前k个指按照特征值的大小从小到大排序得到)λik,以及对应的特征向量Vik。
	(4)把这k个特征（列）向量Vik排列在一起组成一个Nxk的矩阵，将其中每一行看作k维空间中的一个向量，
	   并使用 K-means 算法进行聚类。聚类的结果中每一行所属的类别就是原来 Graph 中的节点亦即最初的个数据点分别所属的类别。
	   或许你已经看出来，谱聚类的基本思想便是利用样本数据之间的相似矩阵（拉普拉斯矩阵）
	进行特征分解（ 通过Laplacian Eigenmap 的降维方式降维），然后将得到的特征向量进行 K-means聚类。
	   此外，谱聚类和传统的聚类方法（例如 K-means）相比，谱聚类只需要数据之间的相似度矩阵就可以了，
	而不必像K-means那样要求数据必须是 N 维欧氏空间中的向量。
	"""
	def initializeGW(self,G, Gepsilon):
		"""
		>>> G = np.arange(5) * np.arange(5)[:, np.newaxis]
		>>> G
		array([[ 0,  0,  0,  0,  0],
		       [ 0,  1,  2,  3,  4],
		       [ 0,  2,  4,  6,  8],
		       [ 0,  3,  6,  9, 12],
		       [ 0,  4,  8, 12, 16]])
		>>> csgraph.laplacian(G, normed=False)
		array([[  0,   0,   0,   0,   0],
		       [  0,   9,  -2,  -3,  -4],
		       [  0,  -2,  16,  -6,  -8],
		       [  0,  -3,  -6,  21, -12],
		       [  0,  -4,  -8, -12,  24]])
		>>> G = np.array([[1,0,1,1,0],[0,1,0,1,1],[1,0,1,1,0],[1,1,1,1,0],[0,1,0,0,1]])
		>>> print G
		[[1 0 1 1 0]
		 [0 1 0 1 1]
		 [1 0 1 1 0]
		 [1 1 1 1 0]
		 [0 1 0 0 1]]
		>>> print csgraph.laplacian(G, normed=False)
		[[ 2  0 -1 -1  0]
		 [ 0  2  0 -1 -1]
		 [-1  0  2 -1  0]
		 [-1 -1 -1  3  0]
		 [ 0 -1  0  0  1]]
		"""
		n = len(self.users)	
 		L = csgraph.laplacian(G, normed = False)#计算G矩阵的拉普拉斯矩阵
 		I = np.identity(n = G.shape[0]) 
 		#Gepsilon是一个系数，在论文中为1
 		GW = I + Gepsilon*L  # W is a double stochastic matrix
 		#print 'GW', GW
		return GW.T

	def getW(self):
		return self.W
	def getW0(self):
		return self.W0
	def getGW(self):
		return self.GW
	def getGW0(self):
		return self.GW0

	def getTheta(self):#获取用户偏置矩阵
		Theta = np.zeros(shape = (self.dimension, len(self.users)))#初始化为行数是特征数，列数为用户总数
		for i in range(len(self.users)):
			Theta.T[i] = self.users[i].theta
		return Theta

	def generateUserFeature(self,W):#生成用户特征
		#TruncatedSVD为sklearn中的函数
		svd = TruncatedSVD(n_components=self.dimension)
		#这里的用户特征是由W矩阵svd得到的，而W矩阵实际上是由用户特征相乘得到的
		#这里其实存在逻辑上的错误
		result = svd.fit(W).transform(W)
		return result#返回的是行数为用户数，列数为特征个数的用户特征矩阵

	def batchRecord(self, iter_):
		print "Iteration %d"%iter_, "Pool", len(self.articlePool)," Elapsed time", datetime.datetime.now() - self.startTime

	def regulateArticlePool(self,pool_list):#从self.articles中随机选出self.poolArticleSize个文章
		# Randomly generate articles
		self.articlePool = []
		for article in self.articles:
			if str(article.id) in pool_list:
				self.articlePool.append(article)

		#self.articlePool = sample(self.articles, self.poolArticleSize)   
		# generate articles 

	def CoTheta(self):#计算一个聚类集中的协同偏置，是用每个用户的偏置乘以用户之间的相似度
		#np.asarray是进行类型转换的，可以将列表，元组形式的转化为数组类型
		for ui in self.users:
			ui.CoTheta = np.zeros(self.dimension)
			for uj in self.users:
				ui.CoTheta += self.W[uj.id][ui.id] * np.asarray(uj.theta)
			#print 'Users', ui.id, 'CoTheta', ui.CoTheta

	def getReward(self, user, Article):
		"""
		预测反馈计算方法是用用户的偏置向量与文章的特征向量相乘
		其中用户的偏置向量用到了聚类的思想，
		是由与目标用户相似的用户集他们的偏置向量与系数相乘决定目标用户的偏置向量
		"""
		reward = np.dot(user.CoTheta, Article.featureVector)
		#print Article.id, reward
		return reward

	def getReward2(self, user, Article):
		"""
		这是针对LinUCB和HyhirdLinUCB算法不使用Cotheta的情况
		"""
		reward = np.dot(user.theta, Article.featureVector)
		#print Article.id, reward
		return reward

	def GetOptimalReward(self, user, articlePool):	
		"""
		>>> sys.float_info.min
		2.2250738585072014e-308
		先将最大反馈初始化为一个极限小的正数这样确保能够有反馈大于这个数，确保能得到一个文章
		"""	
		maxReward = sys.float_info.min#初始化最大反馈
		optimalArticle = None#初始化最优文章
		for x in articlePool:#对文章聚类集中的每一个文章所对应的文章特征都与用户特征	 
			reward = self.getReward2(user, x)
			if reward > maxReward:
				maxReward = reward
				optimalArticle = x
		return maxReward, optimalArticle
	
	def getL2Diff(self, x, y):
		#np.linalg.norm 函数中ord的缺省值为2
		return np.linalg.norm(x-y) #将向量中元素的元素取平方然后求和，最后开平方

	def runAlgorithms(self, algorithms):
		# get cotheta for each user
		
		#记录时间信息
		self.startTime = datetime.datetime.now()
		timeRun = datetime.datetime.now().strftime('_%m_%d') 
		timeRun_Save = datetime.datetime.now().strftime('_%m_%d_%H_%M') 
		
		#保存文件名信息
		fileSig = ''
		filenameWriteRegret = os.path.join(Delicious_result, 'AccRegret' + timeRun_Save + '.csv')
		filenameWritePara = os.path.join(Delicious_result, 'ParameterEstimation' + timeRun_Save + '.csv')
		for alg_name, alg in algorithms.items():
			fileSig = 'New_' +str(alg_name) + '_UserNum'+ str(len(self.users)) + '_Sparsity' + str(self.sparseLevel) +'_Noise'+str(self.noiseLevel)+ '_matrixNoise'+str(self.matrixNoiseLevel)
		filenameWriteResult = os.path.join(Delicious_result, fileSig + timeRun + '.csv')
		print "save result file success"


		self.CoTheta()#计算每个用户的相似用户集影响特征向量
		self.startTime = datetime.datetime.now()

		tim_ = []
		BatchAverageRegret = {}
		AccRegret = {}
		ThetaDiffList = {}
		CoThetaDiffList = {}
		WDiffList = {}
		
		ThetaDiffList_user = {}
		CoThetaDiffList_user = {}
		WDiffList_user = {}
		
		# Initialization
		"""
		这段是初始化算法是否能对性能进行验证，如果可以验证就初始化结果数组
		"""
		for alg_name, alg in algorithms.items():
			BatchAverageRegret[alg_name] = []		
			AccRegret[alg_name] = {}
			if alg.CanEstimateCoUserPreference:
				CoThetaDiffList[alg_name] = []
			if alg.CanEstimateUserPreference:
				ThetaDiffList[alg_name] = []
			if alg.CanEstimateW:
				WDiffList[alg_name] = []

			#记录每个算法下每个用户的预测误差
			for i in range(len(self.users)):
				AccRegret[alg_name][i] = []
		
		userSize = len(self.users)
		'''
		with open(filenameWriteRegret, 'a+') as f:
			f.write('Time(Iteration)')
			f.write(',' + ','.join( [str(alg_name) for alg_name in algorithms.iterkeys()]))
			f.write('\n')
		with open(filenameWritePara, 'a+') as f:
			f.write('Time(Iteration)')
			f.write(',' + ','.join( [str(alg_name)+'CoTheta' for alg_name in algorithms.iterkeys()]))
			f.write(','+ ','.join([str(alg_name)+'Theta' for alg_name in ThetaDiffList.iterkeys()]))
			f.write(','+ ','.join([str(alg_name)+'W' for alg_name in WDiffList.iterkeys()]))
			f.write('\n')
		'''
		process_event = open(process_event_file,'r')
		process_event.readline()

		# Loop begin
		iter_ = 0
		for line in process_event:
			iter_ += 1
			# prepare to record theta estimation error
			#重复声明
			for alg_name, alg in algorithms.items():
				if alg.CanEstimateCoUserPreference:
					CoThetaDiffList_user[alg_name] = []
				if alg.CanEstimateUserPreference:
					ThetaDiffList_user[alg_name] = []
				if alg.CanEstimateW:
					WDiffList_user[alg_name] = []
			#self.regulateArticlePool() # select random articles	
			#for u in self.users:
				#u = choseUser()
				#u = choice(self.users)
				#u = self.users[0]

			userID, tim, articlePool = parseLine_Delicious(line)

			u = self.users[userID-1]

			article_chosen = int(articlePool[0])

			self.regulateArticlePool(articlePool)

			#print self.articlePool
			#pdb.set_trace()



				#self.regulateArticlePool() # 随机选择（初始值为10）个文章，构成文章集

				#noise = self.noise()
				#get optimal reward for user x at time t
				#temp, optimalA = self.GetOptimalReward(u, self.articlePool)#从文章集中选出最优的文章预测分数和文章编号
			OptimalReward = 1.0#预测结果值（含有噪声）

			for alg_name, alg in algorithms.items():
				
				pickedArticle = alg.decide(self.articlePool, u.id)#这一步应该是选择预测评分文章
				
				if pickedArticle.id == article_chosen:
					reward = 1.0
				else:
					reward = 0.0

				if alg_name =='CLUB':#如果是CLUB算法，更新参数和用户聚类集
					alg.updateParameters(pickedArticle.featureVector, reward, u.id)
					n_components= alg.updateGraphClusters(u.id,'False')
				elif alg_name == 'COFIBA':#如果是COFIBA更新参数，用户聚类集和商品聚类集
					alg.updateParameters(pickedArticle.featureVector, reward, u.id)
					itemClusterNum = alg.Iclusters[pickedArticle.id]
					alg.updateUserClusters(u.id, pickedArticle.featureVector, itemClusterNum)#更新用户聚类集分布
					alg.updateItemClusters(u.id, pickedArticle, itemClusterNum, self.articlePool)#更新商品聚类集分布
				else:#其他算法只更新参数
						#print "aaaaaaaa"
						#pdb.set_trace()
					alg.updateParameters(pickedArticle.featureVector, reward, u.id)

				regret = OptimalReward - reward	#计算误差

				AccRegret[alg_name][u.id].append(regret)#计算累计误差

					# every algorithm will estimate co-theta
				if  alg.CanEstimateCoUserPreference:
					CoThetaDiffList_user[alg_name] += [self.getL2Diff(u.CoTheta, alg.getCoTheta(u.id))]
				if alg.CanEstimateUserPreference:
					ThetaDiffList_user[alg_name] += [self.getL2Diff(u.theta, alg.getTheta(u.id))]
				if alg.CanEstimateW:
					WDiffList_user[alg_name] +=  [self.getL2Diff(self.W.T, alg.getW(u.id))]
						#WDiffList_user[alg_name] +=  [self.getL2Diff(self.W.T[u.id], alg.getW(u.id))]

			for alg_name, alg in algorithms.items():
				if alg.CanEstimateCoUserPreference:
					CoThetaDiffList[alg_name] += [sum(CoThetaDiffList_user[alg_name])/userSize]
				if alg.CanEstimateUserPreference:
					ThetaDiffList[alg_name] += [sum(ThetaDiffList_user[alg_name])/userSize]
				if alg.CanEstimateW:
					WDiffList[alg_name] += [sum(WDiffList_user[alg_name])/userSize]
					#WDiffList[alg_name] += [WDiffList_user[alg_name]]
				
			if iter_%self.batchSize == 0:
				self.batchRecord(iter_)
				tim_.append(iter_)
				for alg_name in algorithms.iterkeys():
					"""
					遍历python字典的几种方法，结果都一样
					aDict = {'key1':'value1', 'key2':'value2', 'key3':'value3'}
					print '-----------dict-------------'
					for d in aDict:
						print "%s:%s" %(d, aDict[d])
					
					print '-----------item-------------'
					for (k,v) in aDict.items():
						print '%s:%s' %(k, v)
					#效率最高
					print '------------iteritems---------'
					for k,v in aDict.iteritems():
						print '%s:%s' % (k, v)
					#最笨的方法
					print '---------iterkeys---------------'
					for k in aDict.iterkeys():
						print '%s:%s' % (k, aDict[k])
					
					print '------------iterkeys, itervalues----------'
					for k,v in zip(aDict.iterkeys(), aDict.itervalues()):
 						print '%s:%s' % (k, v)

					"""
					#itervalues是遍历字典中的value
					TotalAccRegret = sum(sum (u) for u in AccRegret[alg_name].itervalues())
					BatchAverageRegret[alg_name].append(TotalAccRegret)
				'''
				with open(filenameWriteRegret, 'a+') as f:
					f.write(str(iter_))
					f.write(',' + ','.join([str(BatchAverageRegret[alg_name][-1]) for alg_name in algorithms.iterkeys()]))
					f.write('\n')
				with open(filenameWritePara, 'a+') as f:
					f.write(str(iter_))
					f.write(',' + ','.join([str(CoThetaDiffList[alg_name][-1]) for alg_name in algorithms.iterkeys()]))
					f.write(','+ ','.join([str(ThetaDiffList[alg_name][-1]) for alg_name in ThetaDiffList.iterkeys()]))
					f.write(','+ ','.join([str(ThetaDiffList[alg_name][-1]) for alg_name in WDiffList.iterkeys()]))
					f.write('\n')
				'''


		#从此往下都是画图部分
		# plot the results		
		f, axa = plt.subplots(2, sharex=True)
		for alg_name in algorithms.iterkeys():	
			axa[0].plot(tim_, BatchAverageRegret[alg_name],label = alg_name)
			with open(filenameWriteResult, 'a+') as f:
				f.write(str(alg_name)+ ','+ str( BatchAverageRegret[alg_name][-1]))
				f.write('\n')

			#plt.lines[-1].set_linewidth(1.5)
			print '%s: %.2f' % (alg_name, BatchAverageRegret[alg_name][-1])
		axa[0].legend(loc='lower right',prop={'size':9})
		axa[0].set_xlabel("Iteration")
		axa[0].set_ylabel("Regret")
		axa[0].set_title("Accumulated Regret")
		
		# plot the estimation error of co-theta
		time = range(self.iterations)
		
		for alg_name, alg in algorithms.items():
			if alg.CanEstimateCoUserPreference:
				axa[1].plot(time, CoThetaDiffList[alg_name], label = alg_name + '_CoTheta')
			#plt.lines[-1].set_linewidth(1.5)
			if alg.CanEstimateUserPreference:
				axa[1].plot(time, ThetaDiffList[alg_name], label = alg_name + '_Theta')
			if alg.CanEstimateW:
				axa[1].plot(time, WDiffList[alg_name], label = alg_name + '_W')
				
		
		axa[1].legend(loc='upper right',prop={'size':6})
		axa[1].set_xlabel("Iteration")
		axa[1].set_ylabel("L2 Diff")
		#axa[1].set_yscale('log')
		axa[1].set_title("Parameter estimation error")

		'''
		for alg_name in algorithms.iterkeys():
			if alg_name == 'WCoLinUCB' or alg_name =='W_W0' or alg_name =='WknowTheta':
				axa[2].plot(time, WDiffList[alg_name], label = alg_name + '_W')
		
		axa[2].legend(loc='upper right',prop={'size':6})
		axa[2].set_xlabel("Iteration")
		axa[2].set_ylabel("L2 Diff")
		axa[2].set_yscale('log')
		axa[2].set_title("Parameter estimation error")
		'''
		plt.savefig(Delicious_result+ '/Delicious_result'+str(timeRun_Save )+'.pdf')
		plt.show()
		#plt.savefig('./SimulationResults/Regret' + str(timeRun_Save )+'.pdf')


if __name__ == '__main__':
	#此部分是进行一些全局常量参数设置
	iterations = 200
	NoiseScale = .1#单一值所加的噪声因素的标准差值为NoiseScale
	matrixNoise = 0.3#矩阵所加的噪声因素的标准差值为matrixNoise

	dimension = 25
	alpha  = 0.2
	lambda_ = 1   # Initialize A 在论文中没有这个参数，所以设值为1
	epsilon = 0 # initialize W
	#eta_ = 0.1

	n_articles = 69226
	ArticleGroups = 5#将全部文章分为5个聚类集

	n_users = 1867#预设用户总数为1847
	UserGroups = 5#将全部用户分为5个聚类集	

	poolSize = 25
	batchSize = 10

	# Parameters for GOBLin
	G_alpha = alpha
	G_lambda_ = lambda_
	Gepsilon = 1
	# Epsilon_greedy parameter
	sparseLevel=10#sparseLevel为选取与目标用户最相似的前sparseLevel个用户，sparseLevel值应该大于0小于元素总数
	RankoneInverse = False
 
	eGreedy = 0.3
	CLUB_alpha_2 = 2.0

	windowSize = 10#这个是在W_Alg中用到的参数，目前还不知是什么作用

	#利用argparse进行参数输入，这里的参数主要是一些可供选择的算法参数，如要跑的算法名，用户数等等
	parser = argparse.ArgumentParser(description = '')
	parser.add_argument('--alg', dest='alg', help='Select a specific algorithm, could be CoLin, GOBLin, AsyncCoLin, or SyncCoLin')
	"""
	parser.add_argument('--iterations', dest = 'iterations', help = 'Set iterations', default = iterations)
	parser.add_argument('--RankoneInverse', action='store_true',
	                help='Use Rankone Correction to do matrix inverse', default = False) 
	parser.add_argument('--userNum', dest = 'userNum', help = 'Set the userNum, can be 40, 80, 100')
	parser.add_argument('--Sparsity', dest = 'SparsityLevel', 
		help ='Set the SparsityLevel by choosing the top M most connected users, should be smaller than userNum, when equal to userNum, we are using a full connected graph')
	parser.add_argument('--NoiseScale', dest = 'NoiseScale', help = 'Set NoiseScale', default = NoiseScale)
	parser.add_argument('--matrixNoise', dest = 'matrixNoise', help = 'Set MatrixNoiseScale', default = matrixNoise)
	"""
	args = parser.parse_args()

	algName = str(args.alg)#算法名，可以是CLUB,COFIBA,CoLin,GOBLin,LinUCB
	"""
	iterations = int(args.iterations)#设置迭代次数
	n_users = int(args.userNum)#用户数量，前面已经设定为40，如果这里缺省的话就是40
	sparseLevel = int(args.SparsityLevel)#sparseLevel为选取与目标用户最相似的前sparseLevel个用户，sparseLevel值应该大于0小于元素总数
	NoiseScale = float(args.NoiseScale)#高斯噪声常量值
	matrixNoise = float(args.matrixNoise)#矩阵中的噪声常量值
	RankoneInverse =args.RankoneInverse#用于计算矩阵的逆矩阵使用，目前还不清楚
	"""
	#sim_files_folder为文件路径，程序把文件读写路径都存入到conf.py下
	userFilename = os.path.join(Delicious_intermediate_result, "users_"+str(n_users)+"+dim_"+str(dimension)+ "+Ugroups_" + str(UserGroups)+".json")
	
	#"Run if there is no such file with these settings; if file already exist then comment out the below funciton"
	# we can choose to simulate users every time we run the program or simulate users once, save it to 'Delicious_intermediate_result', and keep using it.

	#设置类UserManager的对象，UserManager类中主要是对用户进行读写操作和初始设置的函数
	"""
	这里的featureUniform(特征归一化)是程序文件util_functions.py中的函数
	argv={'l2_limit':1} 这个参数在当thetaFunc=gaussianFeature时有用到，其他的时候不用
	argv是一个字典，里面可以写一些gaussianFeature函数内部的参数值，如均值，标准差等等。
	featureUniform和gaussianFeature均在程序文件util_functions.py中
	"""
	UM = UserManager(dimension, userNum = n_users, UserGroups = UserGroups, thetaFunc=featureUniform, argv={'l2_limit':1})
	users = UM.simulateThetafromUsers()#用户偏置向量也设定成初始随机生成
	UM.saveUsers(users, userFilename, force = True)#这里的参数force为False时不允许重复写入，为True运行重复写入
	#users = UM.loadUsers(userFilename)

	articlesFilename = os.path.join(Delicious_intermediate_result, "articles_"+str(n_articles)+"+dim_"+str(dimension) + "+Agroups_" + str(ArticleGroups)+".json")
	# Similarly, we can choose to simulate articles every time we run the program or simulate articles once, save it to 'Delicious_intermediate_result', and keep using it.
	AM = ArticleManager(dimension, n_articles=n_articles, ArticleGroups = ArticleGroups, FeatureFunc=featureUniform,  argv={'l2_limit':1})
	#articles = AM.simulateArticlePool()#文章的特征向量是随机生成的
	articles = AM.getArticles()#写入通过tfidf得到的用户特征向量
	AM.saveArticles(articles, articlesFilename, force=True)#这里的参数force为False时不允许重复写入，为True运行重复写入
	#articles = AM.loadArticles(articlesFilename)
	"""
	lambda 是一个快速创建函数的方法
	>>> f = lambda x: x + 1
	>>> f(3)
	4
	noise = lambda : np.random.normal(scale = 2.0)
	>>>print noise()
	-1.6756562277
	>>>print noise
	<function <lambda> at 0x1075efe60>
	"""
	"""
	>>>np.random.normal(loc=1, #均值,缺省值为0
						scale = 1.0, #标准差,缺省值为1.0
						size=6 #规模,缺省值为None
						)
	[ 1.95123905 -0.01735553 -0.21557828  0.09226065  0.892615    1.44175677]
	"""
	#############################################
	#pdb.set_trace()
	#############################################

	simExperiment = simulateOnlineData(dimension  = dimension,
						iterations = iterations,
						articles=articles,
						users = users,
						#不知道为何不直接传递值，而是创建了一个函数
						#这里的noise不是一个值，而是一个没有输入的函数
						#创建函数的目的是每次调用时，产生的是不同的随机数，如果进行值传递，每次调用时都是固定值
						#函数本身是一个均值为0，标准差为NoiseScale的高斯分布函数
						noise = lambda : np.random.normal(scale = NoiseScale),
						matrixNoise = lambda : np.random.normal(scale = matrixNoise),
						batchSize = batchSize,
						type_ = "UniformTheta", 
						signature = AM.signature,
						poolArticleSize = poolSize, 
						noiseLevel = NoiseScale, #单一值所加的噪声因素的标准差值为noiseLevel
						matrixNoiseLevel= matrixNoise, #矩阵所加的噪声因素的标准差值为matrixNoiseLevel
						epsilon = epsilon, 
						Gepsilon =Gepsilon, 
						sparseLevel= sparseLevel)
	print "Starting for ", simExperiment.simulation_signature
	#userFeature = simExperiment.generateUserFeature(simExperiment.getW())
	#print 'FeatureFunc', userFeature
	"""
	算法中一般并没有用到用户特征向量
	而是通过用户特征向量来计算用户之间的相似度
	根据相似度构建聚类矩阵
	"""
	algorithms = {}
	"""
	W矩阵是用户之间相似度矩阵
	只有GOBLin和CoLin算法使用了W权重矩阵，参看论文：A Gang of Bandits
	GOBLin和CoLin算法是假设提前知道用户之间相似度情况，
	所以本程序是根据用户特征，提前构建用户相似度矩阵W
	但是对于CLUB和COFIBA算法，默认不知道用户的特征，不知道用户之间的相似度情况，只知道商品特征，
	所以没有用到W矩阵
	"""
	if algName == 'LinUCB':
		algorithms['LinUCB'] = LinUCBAlgorithm(dimension = dimension, alpha = alpha, lambda_ = lambda_, n = n_users, RankoneInverse = RankoneInverse)
	if algName == 'GOBLin':#GOBLin的W权重矩阵采用的是laplace变换过的
		algorithms['GOBLin'] = GOBLinAlgorithm( dimension= dimension, alpha = G_alpha, lambda_ = G_lambda_, n = n_users, W = simExperiment.getGW(), RankoneInverse = RankoneInverse )
	if algName =='CoLin':#Colin与CoLinRank1的区别是CoLinRank1的W是在CoLin的W基础上加上噪声并做归一化处理
		algorithms['CoLin'] = CoLinAlgorithm(dimension=dimension, alpha = alpha, lambda_ = lambda_, n = n_users, W = simExperiment.getW(), RankoneInverse = RankoneInverse)
		#algorithms['CoLin_2'] = CoLinAlgorithm_2(dimension=dimension, alpha = alpha, lambda_ = lambda_, n = n_users, W = simExperiment.getW0(), RankoneInverse = RankoneInverse)
	if algName == 'CoLinRank1':
		algorithms['CoLinRank1'] = CoLinRankoneAlgorithm(dimension=dimension, alpha = alpha, lambda_ = lambda_, n = n_users, W = simExperiment.getW0(), RankoneInverse = RankoneInverse)
	if algName == 'HybridLinUCB':
		"""
		Hybrid_LinUCBAlgorithms算法用到了用户特征向量
		但是实际上此用户特征向量是由W矩阵通过svd得到的
		"""
		algorithms['HybridLinUCB'] = Hybrid_LinUCBAlgorithm(dimension = dimension, alpha = alpha, lambda_ = lambda_, userFeatureList=simExperiment.generateUserFeature(simExperiment.getW()))
	if algName =='CLUB':
		algorithms['CLUB'] = CLUBAlgorithm(dimension =dimension,alpha = alpha, lambda_ = lambda_, n = n_users, alpha_2 = CLUB_alpha_2, cluster_init = 'Erdos-Renyi')	
	if algName == 'COFIBA':
		algorithms['COFIBA'] = COFIBAAlgorithm(dimension = dimension, alpha = alpha,alpha_2 = CLUB_alpha_2,lambda_ = lambda_, n = n_users,itemNum= n_articles,cluster_init = 'Erdos-Renyi')
	if algName =='ALL':#由于GOBLin和CoLin需要用到用户特征向量，所以先不运行
		algorithms['LinUCB'] = LinUCBAlgorithm(dimension = dimension, alpha = alpha, lambda_ = lambda_, n = n_users, RankoneInverse = RankoneInverse)
		algorithms['HybridLinUCB'] = Hybrid_LinUCBAlgorithm(dimension = dimension, alpha = alpha, lambda_ = lambda_, userFeatureList=simExperiment.generateUserFeature(simExperiment.getW()),RankoneInverse = RankoneInverse )
		#algorithms['GOBLin'] = GOBLinAlgorithm( dimension= dimension, alpha = G_alpha, lambda_ = G_lambda_, n = n_users, W = simExperiment.getGW(), RankoneInverse = RankoneInverse )
		#algorithms['CoLin'] = CoLinAlgorithm(dimension=dimension, alpha = alpha, lambda_ = lambda_, n = n_users, W = simExperiment.getW(), RankoneInverse=RankoneInverse)
		#algorithms['CLUB'] = CLUBAlgorithm(dimension =dimension,alpha = alpha, lambda_ = lambda_, n = n_users, alpha_2 = CLUB_alpha_2)	
		algorithms['Learn_WBatch_cons'] =  LearnWAlgorithm(dimension = dimension, alpha = alpha, lambda_ = lambda_,  n = n_users, windowSize = windowSize)
 		algorithms['Learn_W-SGD'] =  LearnWAlgorithm_SGD(dimension = dimension, alpha = alpha, lambda_ = lambda_, n = n_users, windowSize=windowSize )
		algorithms['COFIBA'] = COFIBAAlgorithm(dimension = dimension, alpha = alpha,alpha_2 = CLUB_alpha_2,lambda_ = lambda_, n = n_users,itemNum= n_articles,RankoneInverse = RankoneInverse,cluster_init = 'Erdos-Renyi')
		algorithms['CLUB'] = CLUBAlgorithm(dimension =dimension,alpha = alpha, lambda_ = lambda_, n = n_users, alpha_2 = CLUB_alpha_2, cluster_init = 'Erdos-Renyi')	

	if algName == 'LearnW':
		algorithms['Learn_WBatch_cons'] =  LearnWAlgorithm(dimension = dimension, alpha = alpha, lambda_ = lambda_,  n = n_users, windowSize = windowSize)
		algorithms['Learn_W-SGD'] =  LearnWAlgorithm_SGD(dimension = dimension, alpha = alpha, lambda_ = lambda_,  n = n_users, windowSize = windowSize)
	
	#algorithms['eGreedy'] = eGreedyAlgorithm(epsilon = eGreedy)
	#algorithms['UCB1'] = UCB1Algorithm()
	
		
	simExperiment.runAlgorithms(algorithms)



	
