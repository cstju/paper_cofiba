# -*- coding: utf-8 -*-
import cPickle
import numpy as np 
from util_functions import featureUniform, gaussianFeature, fileOverWriteWarning
from random import sample, randint
import json
import scipy.io
from conf import Delicious_feature, Delicious_itemid

#本程序文件下的函数主要是对文章方面的数据进行读写操作

class Article():	
	def __init__(self, aid, FV=None):
		self.id = aid
		self.featureVector = FV
		

class ArticleManager():
	def __init__(self, dimension, n_articles, ArticleGroups, FeatureFunc, argv=None ):
		self.signature = "Article manager for simulation study"
		self.dimension = dimension
		self.n_articles = n_articles
		self.ArticleGroups = ArticleGroups
		self.FeatureFunc = FeatureFunc
		self.argv = argv
		self.signature = "A-"+str(self.n_articles)+"+AG"+ str(self.ArticleGroups)+"+TF-"+self.FeatureFunc.__name__

	def saveArticles(self, Articles, filename, force = False):
		fileOverWriteWarning(filename, force)
		with open(filename, 'w') as f:
			for i in range(len(Articles)):
				#print Articles[i].featureVector
				f.write(json.dumps((Articles[i].id, Articles[i].featureVector.tolist())) + '\n')


	def loadArticles(self, filename):
		articles = []
		with open(filename, 'r') as f:
			for line in f:
				aid, featureVector = json.loads(line)
				articles.append(Article(aid, np.array(featureVector)))
		return articles#返回的articles中的每一行是类Article的一个对象，这么做应该是为了省内存

	#automatically generate masks for articles, but it may generate same masks
	def generateMasks(self):
		mask = {}
		for i in range(self.ArticleGroups):
			mask[i] = np.random.randint(2, size = self.dimension)
		return mask
	"""
	def simulateArticlePool(self):#这里初始化文章特征向量的方法与用户偏置向量方法一样
		articles = []
		
		articles_id = {}
		mask = self.generateMasks()

		for i in range(self.ArticleGroups):
			articles_id[i] = range((self.n_articles*i)/self.ArticleGroups, (self.n_articles*(i+1))/self.ArticleGroups)

			for key in articles_id[i]:
				featureVector = np.multiply(self.FeatureFunc(self.dimension, self.argv), mask[i])
				l2_norm = np.linalg.norm(featureVector, ord =2)
				articles.append(Article(key, featureVector/l2_norm ))
	
		return articles#返回的articles中的每一行是类Article的一个对象，这么做应该是为了省内存
	"""
	def simulateArticlePool(self):#这里初始化文章特征向量的方法与用户偏置向量方法一样
		articles = []

		for i in range(self.n_articles):
			featureVector = self.FeatureFunc(self.dimension, self.argv)
			l2_norm = np.linalg.norm(featureVector, ord =2)
			articles.append(Article(i, featureVector/l2_norm ))
	
		return articles#返回的articles中的每一行是类Article的一个对象，这么做应该是为了省内存
	def getArticles(self):
		articles = []
		articles_id = {}
		articles_id_file = open(Delicious_itemid,'r')
		first_line = articles_id_file.readline()
		for line in articles_id_file:
			arr = line.strip().split('\t')
			articles_id.setdefault(int(arr[0]),int(arr[1]))
		articles_feature_matrix = scipy.io.mmread(Delicious_feature)

		for key in articles_id:
			articles.append(Article(articles_id[key],articles_feature_matrix[key]))
	
		return articles
