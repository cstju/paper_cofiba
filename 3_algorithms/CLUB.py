# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-09 20:30:45
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-13 22:31:36
import numpy as np
from LinUCB import *
import math
from scipy.sparse.csgraph import connected_components
from scipy.sparse import csr_matrix
import pdb
"""
对应论文 Online Clustering of Bandits
中关于用户和商品特征向量的生成只简单提了一下
对于LastFm和delicious数据集是采用tf-idf算法提出主要特征
但并没有说明白
对于Yahoo 数据集是直接采用136维的二值特征表示用户特征
用323维的特征表示商品特征，但是这两个特征维度不一样，不能直接使用
由于没有看过Yahoo数据集，所以对其具体实现不是很清楚
"""
class CLUBUserStruct(LinUCBUserStruct):
	def __init__(self,featureDimension,  lambda_, userID):

		self.userID = userID
		self.A = lambda_*np.identity(n = featureDimension)
		self.b = np.zeros(featureDimension)
		self.AInv = np.linalg.inv(self.A)#对矩阵A取逆
		self.UserTheta = np.zeros(featureDimension)#单一用户的偏置向量，维度为特征个数

		self.reward = 0
		self.CA = self.A
		self.Cb = self.b
		self.CAInv = np.linalg.inv(self.CA)
		self.CTheta = np.dot(self.CAInv, self.Cb)
		self.I = lambda_*np.identity(n = featureDimension)	
		self.counter = 0
		self.CBPrime = 0
		self.d = featureDimension
	def updateParameters(self, articlePicked_FeatureVector, click,alpha_2):
		#LinUCBUserStruct.updateParameters(self, articlePicked_FeatureVector, click)
		#alpha_2 = 1
		self.A += np.outer(articlePicked_FeatureVector,articlePicked_FeatureVector)
		self.b += articlePicked_FeatureVector*click
		self.AInv = np.linalg.inv(self.A)
		self.UserTheta = np.dot(self.AInv, self.b)
		self.counter+=1
		self.CBPrime = alpha_2*np.sqrt(float(1+math.log10(1+self.counter))/float(1+self.counter))

	def updateParametersofClusters(self,clusters,userID,Graph,users):
		self.CA = self.I
		self.Cb = np.zeros(self.d)
		#print type(clusters)

		for i in range(len(clusters)):#对所有聚类集中的用户都进行参数更新
			if clusters[i] == clusters[userID]:#只更新当前用户的参数
				self.CA += float(Graph[userID,i])*(users[i].A - self.I)
				self.Cb += float(Graph[userID,i])*users[i].b
		self.CAInv = np.linalg.inv(self.CA)
		self.CTheta = np.dot(self.CAInv,self.Cb)

	def getProb(self, alpha, article_FeatureVector,time):
		mean = np.dot(self.CTheta, article_FeatureVector)
		var = np.sqrt(np.dot(np.dot(article_FeatureVector, self.CAInv),  article_FeatureVector))
		pta = mean + alpha * var*np.sqrt(math.log10(time+1))
		return pta

class CLUBAlgorithm(LinUCBAlgorithm):
	def __init__(self,dimension,alpha,lambda_,n,alpha_2, cluster_init="Complete"):
		self.n_users = n
		self.time = 0
		#N_LinUCBAlgorithm.__init__(dimension = dimension, alpha=alpha,lambda_ = lambda_,self.n_users=self.n_users)
		self.users = []
		#algorithm have n users, each user has a user structure
		for i in range(self.n_users):
			#print dimension
			#print lambda_
			#print i
			#pdb.set_trace()
			self.users.append(CLUBUserStruct(dimension,lambda_, i)) 

		self.dimension = dimension
		self.alpha = alpha
		self.alpha_2 = alpha_2
		if (cluster_init=="Erdos-Renyi"):
			#Erdos-Renyi是产生随机图谱的一种方法
			p = 3*math.log(self.n_users)/self.n_users
			self.Graph = np.random.choice([0, 1], size=(self.n_users,self.n_users), p=[1-p, p])
			"""
			numpy.random.choice(a, size=None, replace=True, p=None)
				Generates a random sample from a given 1-D array
				Parameters:	
				a : 1-D array-like or int
					If an ndarray, a random sample is generated from its elements. 
					If an int, the random sample is generated as if a was np.arange(n)
				size : int or tuple of ints, optional
					Output shape. If the given shape is, e.g., (m, n, k), 
					then m * n * k samples are drawn. Default is None, 
					in which case a single value is returned.
				replace : boolean, optional
					Whether the sample is with or without replacement
				p : 1-D array-like, optional
					The probabilities associated with each entry in a. 
					If not given the sample assumes a uniform distribution over all entries in a.
			"""
			self.clusters = []
			g = csr_matrix(self.Graph)#将矩阵中非0元素的行列值标出
			N_components, component_list = connected_components(g)#计算相连的元素个数和元素标号，这个函数不是很明白
			self.clusters = component_list
		else:
			self.Graph = np.ones([self.n_users,self.n_users]) #初始化是只有一个聚类集，所有用户都在此集内
			self.clusters = []
			g = csr_matrix(self.Graph)
			N_components, component_list = connected_components(g)
			self.clusters = component_list

		self.CanEstimateCoUserPreference = False
		self.CanEstimateUserPreference = False
		self.CanEstimateW = False
			
	def decide(self,pool_articles,userID):
		self.users[userID].updateParametersofClusters(self.clusters,userID,self.Graph, self.users)
		maxPTA = float('-inf')
		articlePicked = None

		for x in pool_articles:
			x_pta = self.users[userID].getProb(self.alpha, x.featureVector,self.time)
			# pick article with highest Prob
			if maxPTA < x_pta:
				articlePicked = x.id
				featureVectorPicked = x.featureVector
				picked = x
				maxPTA = x_pta

		self.time +=1

		return picked
	def updateParameters(self, featureVector, click,userID):
		self.users[userID].updateParameters(featureVector, click, self.alpha_2)
	def updateGraphClusters(self,userID, binaryRatio):
		n = len(self.users)
		for j in range(n):
			ratio = float(np.linalg.norm(self.users[userID].UserTheta - self.users[j].UserTheta,2))/float(self.users[userID].CBPrime + self.users[j].CBPrime)
			#print float(np.linalg.norm(self.users[userID].UserTheta - self.users[j].UserTheta,2)),'R', ratio
			if ratio > 1:
				ratio = 0
			elif binaryRatio == 'True':
				ratio = 1
			elif binaryRatio == 'False':
				ratio = 1.0/math.exp(ratio)
			#print 'ratio',ratio
			#pdb.set_trace()
			self.Graph[userID][j] = ratio
			self.Graph[j][userID] = self.Graph[userID][j]
		"""
		connected_components函数是计算一个无向图中节点相连情况，把所有相互连接的节点构成一个子集
		N_components展示一共的子集个数
		component_list展示一个子集中包含哪些节点
		"""
		N_components, component_list = connected_components(csr_matrix(self.Graph))
		#pdb.set_trace()
		self.clusters = component_list
		return N_components
	def getLearntParameters(self, userID):
		return self.users[userID].UserTheta





