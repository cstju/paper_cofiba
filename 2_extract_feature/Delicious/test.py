# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-08 22:35:20
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-16 15:34:00
"""
import sklearn
import sys
import re
import numpy as np
import scipy.sparse
import scipy.io
import pdb
from collections import defaultdict 
from sets import Set
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import TruncatedSVD

count = scipy.sparse.lil_matrix( (4,4))
count[0,1] = 1
count[0,3] = 3
count[1,0] = 2
count[1,2] = 4
count[2,2] = 3
count[3,0] = 2
count[3,2] = 1
count[3,3] = 3
tfidf_transformer = TfidfTransformer()
x = tfidf_transformer.fit_transform(count)
print count
print x
print np.array(x)
svd = TruncatedSVD(n_components=2) #Use TruncatedSVD instead of PCA or RandomizedPCA since the matrix is large and sparse.
result = svd.fit(x).transform(x)
print result
"""
from operator import itemgetter
d = {}
d['a'] = 0.1
d['b'] = 0.5
d['c'] = 0.3
for key,value in sorted(d.iteritems(),key = itemgetter(1),reverse=True)[:]:
	print key,value
for i, idx in enumerate(d):
	print i,idx
#fartistidx.close()