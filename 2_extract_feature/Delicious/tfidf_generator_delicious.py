# -*- coding: utf-8 -*-
# @Author: 3dlabuser
# @Date:   2016-11-06 23:14:53
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-16 11:15:37
import sklearn
import sys
import re
import numpy
import scipy.sparse
import scipy.io
import pdb
from collections import defaultdict 
from sets import Set
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.decomposition import TruncatedSVD

root_path = '/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/paper_COFIBA/'
relationFileName = root_path+'1_data_preprocess/Delicious_dataset/new_data/'

tags_file = relationFileName+'tags.dat'
user_tag_bookmark_timestamp_file = relationFileName+'user_taggedbookmarks-timestamps.dat'

Delicious_feature = './feature_result/'

#pre-process events
file = open(user_tag_bookmark_timestamp_file, 'r')
first_line = file.readline()#这一步是为了跳过表头那一行
raw_user_bookmark_tag = []
for line in file:
	arr = line.strip().split('\t')
	#print arr
	#pdb.set_trace()
	t = {}
	t['uid'] = int(arr[0])
	t['aid'] = int(arr[1])
	t['tid'] = int(arr[2])
	t['tstamp'] = int(arr[3])
	raw_user_bookmark_tag.append(t)

print 'raw event number: '+str(len(raw_user_bookmark_tag))
file.close()
#remove tags occuring less than 10
ftagremove = open(relationFileName+'tag_remove.dat','w')
tag_count = {}
tag_remove = Set([])
for t in raw_user_bookmark_tag:#进行标签统计
	if t['tid'] not in tag_count:
		tag_count.setdefault(t['tid'],0)
	tag_count[t['tid']] += 1

for tid in tag_count:#对tag_count字典中的值进行查找
	if tag_count[tid] < 10:#删除出现次数小于10次的标签
		ftagremove.write(str(tid)+'\n')
		tag_remove.add(tid)

file = open(tags_file, 'r')
file.readline()
for line in file:
	arr = line.strip().split('\t')
	#print arr
	#pdb.set_trace()
	if '?' in arr[1] and int(arr[0]) not in tag_remove:#删除标签里带有“？”的标签
		#print arr[0]
		ftagremove.write(str(arr[0])+'\n')
		tag_remove.add(int(arr[0]))
file.close()
ftagremove.close()

print 'removed tags: '+str(len(tag_remove))
#pdb.set_trace()
#build vocabulary



vocab = {}
tag2word = {}
file = open(tags_file, 'r')
file.readline()
for line in file:
	arr = line.strip().split('\t')
	#print arr 
	#pdb.set_trace()	
	if  int(arr[0]) not in  tag_remove:		
		wlist = re.split(r"[ _-]+", arr[1])	#正则化分解
		#print wlist
		if int(arr[0]) not in tag2word:
			tag2word[int(arr[0])] = wlist
			#print int(arr[0])
			#print wlist
			#pdb.set_trace()
		for word in wlist:
			if not word in vocab:
				vocab[str(word)] = len(vocab)#对tag描述中的词进行编号
		#print arr[0]+' '+str(tag2word[int(arr[0])])
print 'tag2word size: '+str(len(tag2word))
print 'vocabulary size: '+str(len(vocab))
file.close()


#print tag2word[0]
#print tag2word[1]


#存储的是保存的标签的的用户_书签_标签_时间戳
file_filter = open(relationFileName+'user_taggedbookmarks-timestamps_filter.dat','w')
#存储的是被删除的标签的的用户_书签_标签_时间戳
file_remove = open(relationFileName+'user_taggedbookmarks-timestamps_remove.dat','w')

file_filter.write(first_line)
file_remove.write(first_line)

num = 0
logs = []
for i,t in enumerate(raw_user_bookmark_tag):
	if int(t['tid']) in tag_remove:#删除的标签		
		num += 1
		file_remove.write(str(t['uid'])+'\t'+str(t['aid'])+'\t'+str(t['tid'])+'\t'+str(t['tstamp'])+'\r\n')#写入到删除的文件中
	else:
		file_filter.write(str(t['uid'])+'\t'+str(t['aid'])+'\t'+str(t['tid'])+'\t'+str(t['tstamp'])+'\r\n')#写入到过滤文件中
		logs.append(t)
		#ftmp.write(str(t)+'\n')
print 'removed event: '+str(num)
file_filter.close()
file_remove.close()

#将logs按照aid从小到大排序
logs2 = sorted(logs,key = lambda t:t['aid'])#将原logs按照书签id，从小到大排序

#build index & document
bookmark2idx = {}
idx2bookmark = []
for i,t in enumerate(logs2):
	if not t['aid'] in bookmark2idx:
		bookmark2idx[t['aid']] = len(bookmark2idx)#对书签进行编号
		idx2bookmark.append(t['aid'])
		#print str(bookmark2idx[t['aid']])+' '+str(len(idx2bookmark))	

#counting frequency 
#x = numpy.zeros(shape = (len(bookmark2idx), len(vocab)))
count = scipy.sparse.lil_matrix( (len(bookmark2idx),len(vocab)))
for t in logs2:
	#print t
	aidx = bookmark2idx[t['aid']]
	#if t['aid'] == 1:
	#	print tag2word[t['tid']]
	#print int(t['tid'])
	#pdb.set_trace()
	#print tag2word[int(t['tid'])]
	#print type(tag2word[int(t['tid'])])
#dict= sorted(dic.iteritems(), key=lambda d:d[1], reverse = True)
	for word in tag2word[int(t['tid'])]:
		count[aidx, vocab[word]] += 1

#get tfidf
tfidf_transformer = TfidfTransformer()
x = tfidf_transformer.fit_transform(count)
print 'tfidf size: '+str(x.shape)

svd = TruncatedSVD(n_components=25) #Use TruncatedSVD instead of PCA or RandomizedPCA since the matrix is large and sparse.
result = svd.fit(x).transform(x)
print 'feature matrix size: '+str(result.shape)
#print to file
fvectors = open(Delicious_feature+'feature_vectors.dat','w')
scipy.io.mmwrite(fvectors, result) #To read the matrix back, use scipy.io.mmread()使用mmread读时，直接返回一个矩阵
fvectors.close()
fartistidx = open(Delicious_feature+'arm_idx.dat','w')
fartistidx.write("index"+'\t'+"armId"+'\n')
for i, idx in enumerate(idx2bookmark):
	fartistidx.write(str(i)+'\t'+str(idx)+'\n')
fartistidx.close()
