# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-10 20:19:19
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-10 21:11:25
import sys
import matplotlib.pyplot as plt
import numpy as np

labelfile = './results/KMeans_200.dat'

# read cluster label
label = []
for i in range(200):
	label.append((0, i))
with open(labelfile,'r') as fin:
	for line in fin:		
	    label[int(line)] = (label[int(line)][0]+1, label[int(line)][1])

label.sort()
label.reverse()
print [x[1] for x in label]
plt.plot(range(1, 201), [x[0] for x in label])
plt.show()