# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-09 23:04:36
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-13 15:17:10
"""
本程序是将原有的用户编号进行从新排序，
由于在数据预处理中已经对编号进行从新排序了
故本程序实际没有用到
"""
user2idx = {}
num = 1
fin = open('user_taggedbookmarks-timestamps.dat','r')
fin.readline()
for line in fin:
	user = line.split()[0]
	if not user in user2idx:
		user2idx[user] = str(num)
		num += 1
fin.close()

fin = open('tag_removed_log.dat','r')
fout = open('tag_removed_log.dat.mapped','w')
fout.write(fin.readline())
for line in fin:
	user = line.split()[0]
	fout.write(user2idx[user]+line[len(user):])
fout.close()
fin = open('user_contacts.dat','r')
fout = open('user_contacts.dat.mapped','w')
fout.write(fin.readline())
for line in fin:
	user1 = line.split()[0]
	user2 = line.split()[1]
	fout.write(user2idx[user1]+'\t'+user2idx[user2]+line[len(user1)+len(user2)+1:])