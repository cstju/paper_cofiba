# -*- coding: utf-8 -*-
# @Author: mac@lab538
# @Date:   2016-11-10 20:19:19
# @Last Modified by:   mac@lab538
# @Last Modified time: 2016-11-11 10:19:49
import time
import sys
import os
import numpy as np
from sklearn import cluster
import pdb
def initializeW_clustering(n,relationFileName):
    W = np.identity(n+1)
    with open(relationFileName) as f:
        f.readline()
        for line in f:
            line = line.split('\t')            
            if int(line[0])<=n and int(line[1]) <=n:
                W[int(line[0])][int(line[1])] +=1 
    return W
root_path = '/Users/3dlabuser/Documents/python_working_directory/recsys/recsys_learn/paper_COFIBA/'
relationFileName = root_path+'1_data_preprocess/Delicious_dataset/new_data/user_contacts-timestamps.dat'
save_path = './results/'
n = 1866
W = initializeW_clustering(n, relationFileName)



ms = cluster.MeanShift()
clustering_names = ['MeanShift']
clustering_algorithms = [ms]

for nClusters in [50, 100, 200]:
    spc = cluster.SpectralClustering(n_clusters=nClusters, affinity="nearest_neighbors")
    clustering_names.append('SpectralClustering_'+str(nClusters))
    #print clustering_names
    clustering_algorithms.append(spc)

for nClusters in [50, 100, 200]:
    kmeans = cluster.KMeans(n_clusters=nClusters)
    clustering_names.append('KMeans_'+str(nClusters))
    clustering_algorithms.append(kmeans)

for name, algorithm in zip(clustering_names, clustering_algorithms):
    print ("Start "+name)
    t0 = time.time()
    algorithm.fit(W)
    t1 = time.time()
    print (name, t1-t0)
    label = algorithm.labels_
    with open(save_path+str(name)+'.dat','w') as f:
        for i in range(len(label)):
            f.write(str(label[i])+'\n')

