本文件夹下存储的是用户聚类相关代码和结果
采用三种聚类方法（1）meanshift（2）spectralcluster（3）kmeans
生成的文件中的数字含义为
行数减一为用户id，
生成的文件中的数字含义为，该用户最终划分的聚类集编号


1）adjacency_list_generator.py生成的文件是user_relation_adjacency_list.dat
此数据集存储的是用户和用户之间的连接关系
第一行代表： 总用户数+空格+总连接数
第二行代表用户id为1的用户，与他相连的其他id，不同id之间用空格分开
第三行代表用户id为2的用户，与他相连的其他id，不同id之间用空格分开
以此类推
。
。
。
。

2）user_clustering.py生成的文件在results文件夹下
user_clustering采用三种聚类方法（1）meanshift（2）spectralcluster（3）kmeans
生成的文件如“KMeans_50.dat”代表采用Kmeans聚类方法共聚了50个类别
“KMeans_50.dat”文件中的数字含义为
行数代表用户id，生成的文件中的数字含义为，该用户最终划分的聚类集编号


3）usernum_cluster_statistics.py 
是对usernum_clustering.py的结果进行画图