arm_idx.dat:
矩阵中的行与书签id对应关系，
由于删除了出现次数小于10次的标签，导致某些书签也被删除
书签总数由原来的69226降为66659
arm_idx.dat文件中第第一列为矩阵中的行编号（从0开始，到66658），
第二列为对应的书签id（从1开始，不连续，到69226）
例如：
由于书签id为1020的书签被删除，
所以矩阵中的第1019行的特征实际对应的是书签id为1021的书签

1019	1021



feature_vectors.dat:
	Feature matrix which reduced column dimension to 25.
	Use scipy.io.mmread() to read the file.