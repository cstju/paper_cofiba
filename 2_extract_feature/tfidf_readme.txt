tfidf算法在本项目中主要用于特征提取，构建商品的特征向量
Movielens，Last.fm， Delicious数据集中都包含三个维度：用户，商品，标签
在Movielens数据集中，商品（item）是电影（movie）
在Last.fm数据集中，商品（item）是艺人（artist）
在Delicious数据集中，商品（item）是链接（url）
tfidf算法的目的是构建商品的n维特征向量，主要思路如下：

1、先将用户id，商品id，标签id，整理成从0开始，连续编号
2、根据每个数据集中的tags.dat文件对标签进行一个事先处理，将相似的标签进行合并
3、根据每个数据集中的user_taggeditems-timestamp.dat文件统计每个item的标签类别及权重
对于Movielens和Delicious数据集，也可使用item_tags.dat文件直接进行统计
4、构建item_tags矩阵，对其使用svd算法提取前n个主成分作为该商品的特征

最终采用的是scipy.io.mmwrite(fvectors, result) 函数进行矩阵的写入
使用 scipy.io.mmread()使用mmread读时，直接返回一个矩阵